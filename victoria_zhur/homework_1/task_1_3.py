edge = 5

volume = edge ** 3
lateral_area = 4 * edge ** 2

print(volume, lateral_area)
