from statistics import mean, geometric_mean

numbers = [2, 3]
arithmetic_avg = mean(numbers)
geometric_avg = geometric_mean(numbers)

print(arithmetic_avg, geometric_avg)
